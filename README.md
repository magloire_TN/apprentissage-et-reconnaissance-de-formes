Fichiers crées le 26 janvier 2017:

Projet : 

1 ) Arbres de décision, sélection de modèles

fichiers :
--> decisiontree
--> fichier imdl_extrait.pkl (utile pour les tests ==> Pas encore télécharger ! )
	Avec Possibilité d'utiliser "GenArti()"

2) Classification bayésienne, Estimation de densité

fichiers : 
--> tme2_velib.ipynb
--> velib.npz

3) Régression linéaire,  logistique, descente de gradient

fichiers :
--> tme3-etu.ipynb
--> USPS.zip

4)  Perceptron

fichiers :
--> semaine_4New.ipynb
--> arftools.py

5)  suite du 4) + implementation réseaux de neurones en cours ...

fichiers :
-->tme5-etu.ipynb + arftools.py

6) SVM, Kernels 

fichiers :
--> tme6.ipynb + arftools.py

7)SVM, Kernels, Bagging, Boosting

fichiers :
--> tm7.ipynb + arftools.py

8) 
fichiers :
--> 
